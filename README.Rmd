---
output: github_document
---

## Overview

The aim of this package is to provide easy access to the LivWell database (link to paper and zenodo). 

The `livwell_indicators` can be used to search for available indicators using string matching and the `livwell_data` function allows simple selection and filtering of data.

## Installation

The easiest way to install the package is using the remotes (or devtools) package:

```{r eval=FALSE}

remotes::install_git("https://gitlab.pik-potsdam.de/belmin/livwelldata.git")

```

## Usage

```{r, eval=FALSE}


library(livwelldata)


myindicators = livwell_indicators(category_contains = c("household", "indiv"),
                   code_contains = "dm",
                   desc_contains = "living")


mydata = livwell_data(indicator_codes = c("ER_elec_rural_p", "HH_watch_p", "HH_toilet_low_p"),
              years = c(1997, 2010),
              country_codes = c("MOZ", "MDG"),
              output_format = "long",
              interpolate = "linear")

mycountries = livwell_countries()

myregions = livwell_harmonized_regions()

```

