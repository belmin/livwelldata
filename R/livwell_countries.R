#' Convenience function to return all country codes and names in dataset
#'
#' @return returns data.frame containing all country codes and names
#' @export
#'
#' @examples
#' livwell_countries()
#' @importFrom rlang .data
#' @import utils
livwell_countries <- function() {
  livwell %>%
    dplyr::group_by(.data$country_code) %>%
    dplyr::summarise(country_name = dplyr::first(.data$country_name))
}

utils::globalVariables(c("livwell"))

